-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2015 at 09:28 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `food_bank_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_meal_entry_tb`
--

CREATE TABLE IF NOT EXISTS `daily_meal_entry_tb` (
  `eating_date` varchar(20) NOT NULL,
  `member_name` varchar(20) NOT NULL,
  `daily_meal` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_meal_entry_tb`
--

INSERT INTO `daily_meal_entry_tb` (`eating_date`, `member_name`, `daily_meal`) VALUES
('2015-09-03', 'islam hossain', '0'),
('2015-09-03', 'hamba', '0'),
('2015-09-03', 'myName', '0'),
('2015-09-03', 'hamba', '0'),
('9/3/2015', 'bumba', '5'),
('0000-00-00', 'islam', '0'),
('0000-00-00', 'rahul', '0'),
('0000-00-00', 'test1', '0'),
('0000-00-00', 'test3', '0'),
('2015-04-21', 'islam', '1'),
('2015-04-21', 'rahul', '1'),
('2015-04-21', 'test1', '1'),
('2015-04-21', 'test3', '1'),
('2015-04-21', 'islam', '1'),
('2015-04-21', 'test3', '1'),
('2015-04-21', 'test1', '1'),
('2015-04-21', 'islam', '1'),
('2015-04-21', 'test3', '1'),
('2015-04-21', 'test1', '1'),
('2015-04-21', 'test1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `food_bank_cost_tb`
--

CREATE TABLE IF NOT EXISTS `food_bank_cost_tb` (
  `member_name` varchar(20) NOT NULL,
  `cost_date` varchar(10) NOT NULL,
  `cost_amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_bank_cost_tb`
--

INSERT INTO `food_bank_cost_tb` (`member_name`, `cost_date`, `cost_amount`) VALUES
('islam', '20/4/2015', 0),
('rahul', '20/4/2015', 0),
('test1', '20/4/2015', 0),
('test3', '20/4/2015', 0),
('islam', '2015-04-21', 100);

-- --------------------------------------------------------

--
-- Table structure for table `food_bank_member_account_tb`
--

CREATE TABLE IF NOT EXISTS `food_bank_member_account_tb` (
  `member_name` varchar(20) NOT NULL,
  `payment_date` varchar(10) NOT NULL,
  `member_amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_bank_member_account_tb`
--

INSERT INTO `food_bank_member_account_tb` (`member_name`, `payment_date`, `member_amount`) VALUES
('islam', '20/4/2015', 0),
('rahul', '20/4/2015', 0),
('test1', '20/4/2015', 0),
('test3', '20/4/2015', 0);

-- --------------------------------------------------------

--
-- Table structure for table `food_bank_member_meal_list_tb`
--

CREATE TABLE IF NOT EXISTS `food_bank_member_meal_list_tb` (
  `entry_date` varchar(20) NOT NULL,
  `member_name` varchar(20) NOT NULL,
  `meal_amount` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_bank_member_meal_list_tb`
--

INSERT INTO `food_bank_member_meal_list_tb` (`entry_date`, `member_name`, `meal_amount`) VALUES
('2015-09-03', 'islam hossain', '0'),
('2015-09-03', 'islam hossain', '0'),
('2015-09-03', 'hamba', '0'),
('2015-09-03', 'myName', '0'),
('2015-09-03', 'hamba', '0');

-- --------------------------------------------------------

--
-- Table structure for table `food_bank_member_tb`
--

CREATE TABLE IF NOT EXISTS `food_bank_member_tb` (
  `member_name` varchar(20) NOT NULL,
  `member_des` varchar(20) NOT NULL,
  `reg_date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_bank_member_tb`
--

INSERT INTO `food_bank_member_tb` (`member_name`, `member_des`, `reg_date`) VALUES
('islam hossain', 'student', '2015-09-03'),
('islam hossain', 'student', '2015-09-03'),
('islam hossain', 'student', '2015-09-03'),
('hamba', 'student', '2015-09-03'),
('myName', 'student', '2015-09-03'),
('hamba', 'student', '2015-09-03'),
('islam', 'student', '20/4/2015'),
('rahul', 'student', '20/4/2015'),
('test1', 'Dhaka City College', '20/4/2015'),
('test3', 'get_fault', '20/4/2015');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
